## Arive Task

I have made it easier for you to run the app using docker

``docker-compose up``

You can also run the app by running

``npm run start:dev``

You can change configs inside `/config/default.yaml`

You can run tests by issuing `npm run test`

You can navigate to docs at: ```http://localhost:4000/api-docs```