import UserService from '../src/services/users.service'
import HobbiesService from '../src/services/hobbies.service'
import mongoose from 'mongoose';

beforeAll(async () => {

  await mongoose.connect('mongodb://localhost:27017/arive_test', {
    useNewUrlParser: true,
  })
})

describe('Creating users and hobbies', () => {
  let user;
  test("Creating New User", async () => {
    user = await UserService.create({
      name: 'OMAR'
    })
    expect(user.name).toBe('OMAR');
  })

  test("Creating New Hobby for a user", async () => {
    let hobby = await HobbiesService.create({
      name: 'Swimming',
      passionLevel: 'low',
      year: 1996,
      userId: user._id
    })
    expect(hobby.userId).toBe(user._id);
  })


})


afterAll(async () => {
  await mongoose.disconnect();
})