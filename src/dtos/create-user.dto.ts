import {
  validate,
  validateOrReject,
  Contains,
  IsInt,
  Length,
  IsEmail,
  IsFQDN,
  IsDate,
  Min,
  Max, IsNotEmpty, IsString, IsOptional, IsDateString,
} from 'class-validator';

export class CreateUserDto {

  constructor(body) {
    this.name = body.name
  }
  @IsString()
  @IsNotEmpty()
  name: string;
}