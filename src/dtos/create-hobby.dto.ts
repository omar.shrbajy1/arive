import {
  IsNotEmpty, IsString, IsNumber, IsEnum,
} from 'class-validator';
import {PassionLevelEnum} from '../models/hobbies.model';

export class CreateHobbyDto {

  constructor(body) {
    this.passionLevel = body.passionLevel
    this.name = body.name;
    this.year = body.year;
    this.userId = body.userId;
  }

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsEnum(PassionLevelEnum)
  passionLevel: string;

  @IsNumber()
  year: number;

  @IsNotEmpty()
  userId: string;

}