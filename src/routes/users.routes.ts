import * as express from 'express';
import UserController from '../controllers/users.controller';
import HobbyController from '../controllers/hobbies.controller';

const router = express.Router();

router.post('/users', UserController.create)
router.get('/users', UserController.get)
router.post('/hobbies', HobbyController.create)
router.get('/hobbies', HobbyController.getByUserId)
router.delete('/hobbies/:hobbyId', HobbyController.delete)

export default router;