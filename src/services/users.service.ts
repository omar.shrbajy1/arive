import {User, IUser} from "../models/users.model";
import {CreateUserDto} from "../dtos/create-user.dto";
import {validate} from "class-validator";
import {CustomError} from '../utils/custom-error';

class UsersService {
  async create(body: CreateUserDto): Promise<IUser> {
    const userDTO = new CreateUserDto(body);
    const errors: any = await validate(userDTO)
    if (errors.length) throw new CustomError('common.validationError', errors[0].constraints);
    const user = await User.create(userDTO)
    return user
  }

  async get(){
    return User.find();
  }


}


export default new UsersService();