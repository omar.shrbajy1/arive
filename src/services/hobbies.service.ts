import {CreateUserDto} from "../dtos/create-user.dto";
import {validate} from "class-validator";
import {CustomError} from '../utils/custom-error';
import {CreateHobbyDto} from "../dtos/create-hobby.dto";
import {Hobby, IHobby} from "../models/hobbies.model";
import {User} from "../models/users.model";
import {Types} from 'mongoose';

class HobbyService {
  async create(body: CreateHobbyDto): Promise<IHobby> {
    const hobbyDTO = new CreateHobbyDto(body);
    const errors: any = await validate(hobbyDTO)
    if (errors.length) throw new CustomError('common.validationError', errors[0].constraints);
    const user = await User.findById(hobbyDTO.userId);
    if (!user) throw new CustomError('user.wrongUserId', null);
    const hobby = await Hobby.create(hobbyDTO)
    return hobby
  }

  getByUserId(userId: string) {
    let condition = {};
    if (userId) {
      if (!Types.ObjectId.isValid(userId)) throw new CustomError('common.invalidId', null);
      condition = {userId};
    }
    return Hobby.find({
      ...condition
    })
  }

  async delete(hobbyId: string): Promise<void> {
    if (!Types.ObjectId.isValid(hobbyId)) throw new CustomError('common.invalidId', null);
    await Hobby.deleteOne({
      _id: hobbyId
    })
    return null;
  }
}


export default new HobbyService();