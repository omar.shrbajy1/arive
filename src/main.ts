import express from "express";
import UserRouter from './routes/users.routes'
import bodyParser from 'body-parser';
import {CustomError} from "./utils/custom-error";
import config from './utils/config';
import mongoose from 'mongoose';
import YAML from 'yamljs';
import swaggerUi from 'swagger-ui-express';

var path = require('path');
var swagger_path = path.resolve(__dirname, './swagger.yaml');

const swaggerDocument = YAML.load(swagger_path);


const app = express();
const port = config.port || 4000; // default port to listen

app.use(bodyParser());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use("/api", UserRouter);


app.use((err, req, res, next) => {

  const errorObj = {
    message: 'internal_server_error',
    statusCode: 500,
    code: 500,
    details: null
  }
  if (err instanceof CustomError) {
    errorObj.message = err.message;
    errorObj.statusCode = err.statusCode;
    errorObj.details = err.details;
    errorObj.code = err.code;
  }
  return res.status(errorObj.statusCode).send(errorObj);
})

app.use(function (req, res, next) {
  res.status(404);
  res.send({
    message: 'not_found'
  })
});

// start the Express server
app.listen(port, async () => {
  try {
    await mongoose.connect(`mongodb://${process.env.DB_HOST || config.db.host}:${config.db.port}/${config.db.database}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    })
  } catch (e) {
    console.log('e', e)
  }
  console.log(`server started at http://localhost:${port}`);
});
