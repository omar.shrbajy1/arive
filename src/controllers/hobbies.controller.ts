import HobbyService from '../services/hobbies.service';
import {Response} from "express";

class HobbyController {


  async create(req, res, next): Promise<Response> {
    try {
      const {body} = req;
      return res.send(await HobbyService.create(body));
    } catch (e) {
      return next(e)
    }
  }


  async getByUserId(req, res, next): Promise<Response> {
    try {
      const {userId} = req.query;
      return res.send(await HobbyService.getByUserId(userId));
    } catch (e) {
      return next(e)
    }
  }


  async delete(req, res, next): Promise<Response> {
    try {
      const {hobbyId} = req.params;
      return res.send(await HobbyService.delete(hobbyId));
    } catch (e) {
      return next(e)
    }
  }

}


export default new HobbyController();