import UserService from '../services/users.service';
import {Response} from "express";

class UserController {

  async create(req, res, next): Promise<Response> {
    try {
      const {body} = req;
      return res.send(await UserService.create(body));
    } catch (e) {
      return next(e)
    }
  }

  async get(req, res, next): Promise<Response> {
    return res.send(await UserService.get());
  }


}


export default new UserController();