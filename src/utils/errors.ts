export default {
  common: {
    validationError: {
      statusCode: 400,
      code: 100,
      message: 'fields_required'
    },
    notFound: {
      statusCode: 404,
      code: 101,
      message: 'item_not_found'
    },
    unauthorized: {
      statusCode: 401,
      code: 102,
      message: 'unauthorized'
    },
    invalidId:{
      statusCode: 400,
      code: 103,
      message: 'invalid_id'
    }
  },
  user: {
    alreadyExist: {
      statusCode: 400,
      code: 201,
      message: 'user_already_exist'
    },
    wrongUserId: {
      statusCode: 400,
      code: 202,
      message: 'wrong_user_id'
    }
  },
}