import {model, Schema, Model, Document} from 'mongoose';

interface IUser extends Document {
  name: string;
  hobbies: string[];
}

const UserSchema: Schema = new Schema({
  name: {type: String, required: true},
});

const User = model<IUser>('User', UserSchema);
export {User, IUser};
