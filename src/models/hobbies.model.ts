import {model, Schema, Model, Document} from 'mongoose';

enum PassionLevelEnum {
  Low = 'low',
  Medium = 'medium',
  High = 'high',
  VeryHigh = 'veryHigh'
}

interface IHobby extends Document {
  name: string;
  passionLevel: PassionLevelEnum;
  year: Number;
  userId: string;

}

const HobbySchema: Schema = new Schema({
  name: {type: String, required: true},
  passionLevel: {type: String, enum: PassionLevelEnum, required: true},
  year: {type: Number, required: true},
  userId: {type: Schema.Types.ObjectId, ref: 'User'}
});

const Hobby = model<IHobby>('Hobby', HobbySchema);
export {Hobby, IHobby, PassionLevelEnum};
